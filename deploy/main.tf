terraform {
  backend "s3" {
    bucket         = "devops-django-app-tfstate"
    key            = "devops-django.tfstate"
    region         = "us-east-2"
    encrypt        = true
    dynamodb_table = "devops-django-tf-state-lock"
  }
}

provider "aws" {
  region  = "us-east-2"
  version = "~> 2.50.0"
}

locals {
  prefix = "${var.prefix}-${terraform.workspace}"
  common_tags = {
    Environment = terraform.workspace
    Project     = var.project
    Owner       = var.contact
    ManagedBy   = "Terraform"
  }
}

data "aws_region" "current" {}
