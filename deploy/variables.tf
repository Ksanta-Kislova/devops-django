variable "prefix" {
  default = "blog"
}

variable "project" {
  default = "devops-blog"
}

variable "contact" {
  default = "Ksanta-kiss@mail.ru"
}

variable "db_username" {
  description = "Username for the RDS Postgres instance"
}

variable "db_password" {
  description = "Password for the RDS postgres instance"
}

variable "bastion_key_name" {
  default = "django-ohio-key"
}

variable "ecr_image_api" {
  description = "ECR Image for API"
  default     = "298414249165.dkr.ecr.us-east-2.amazonaws.com/devops-django:latest"
}

variable "ecr_image_proxy" {
  description = "ECR Image for API"
  default     = "298414249165.dkr.ecr.us-east-2.amazonaws.com/django-proxy:dev"
}

variable "django_secret_key" {
  description = "Secret key for Django app"
}
